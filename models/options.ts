export class Options {

    public limits: boolean = true;
    public columns: boolean = true;
    public save: boolean = true;

    public print: boolean = true;
    public excel: boolean = true;

    public search: boolean = true;

    public create: boolean = true;
    public sort: boolean = true;
    public edit: boolean = true;
    public delete: boolean = true;

    public paging: boolean = true;
    public info: boolean = true;


    public titleColumns: boolean = true;

    public classStyle: string;
}
