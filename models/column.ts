export class Column {
    public width: string;
    public title: string;
    public field: string;
    public func: (row: any) => string;
    public sortable: boolean = true;
    public isVisible: boolean = true;
    public constructor(title: string, field: string | ((row: any) => string)) {
        this.title = title;
        if (typeof field == 'string') this.field = field;
        else this.func = field;
    }

    public getValue(row: any): string {
        if(this.func) return this.func(row);
        return row[this.field];
    }
}